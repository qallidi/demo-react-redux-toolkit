import pokemonReducer, {updateCurrentValue} from "../features/pokemon/pokemonSlice";

describe("pokemon reducer", () => {
    const initialState = {
        currentValue: null, results: [], status: null, errors: []
    };

    it("should handle initial state", () => {
        expect(pokemonReducer(undefined, { type: "unknown" })).toEqual(initialState);
    });

    it("should handle update current value", () => {
        const actual = pokemonReducer(initialState, updateCurrentValue("charizard"));
        expect(actual.currentValue).toEqual("charizard");
    });
})