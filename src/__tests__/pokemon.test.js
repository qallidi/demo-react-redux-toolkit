import React from "react";
import renderer from "react-test-renderer";
import {Provider} from "react-redux";
import Pokemon from "../features/pokemon/Pokemon";
import {store} from "../app/store";

test("PokemonUI rendered correctly", () => {
    const PokemonUI = <Provider store={store}>
        <Pokemon/>
    </Provider>;
    const tree = renderer.create(PokemonUI).toJSON();
    expect(tree).toMatchSnapshot();
})