import {Paper, styled} from "@mui/material";
import {themeColors} from "./theme";

export const Item = styled(Paper)(({theme}) => ({
    backgroundColor: theme.palette.mode === "dark" ? themeColors.black : themeColors.white, ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
}));