export const themeColors = {
    orangeRed: "#FF4500",
    cihRed: "#DA5A27",
    orange: "#ffb191",
    gold: "#FFD700",
    springGreen: "#00FF7F",
    darkblue: "#1f0776",
    dodgerBlue: "#1E90FF",
    cornFlowerBlue: "#6495ED",
    blue: "#00A0E7",
    cihBlue: "#00A0E7",
    gray: "#808080",
    darkGray: "#A9A9A9",
    silver: "#C0C0C0",
    white: "#FFFFFF",
    black: "#000000"
};
export const themeShadows = {
    nav: "0px 4px 20px rgb(0 160 231 / 5%)",
    card: "0px 4px 20px rgb(0 160 231 / 5%);",
};