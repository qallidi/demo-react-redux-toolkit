import React, {lazy, Suspense} from "react";
import Spinner from "../libs/BasicSpinner";
import {Grid} from "@mui/material";
import Box from "@mui/material/Box";
import {Item} from "../assets/StyledItem";
import Result from "../features/pokemon/Result";
// import component pokemon using lazy
const Pokemon = lazy(() => import("../features/pokemon/Pokemon"));

export default function MainLayout() {

    return (<Box sx={{width: "100%"}}>
        <br/>
        <br/>
        <Grid container rowSpacing={3} columnSpacing={{xs: 1, sm: 2, md: 3}}>
            <Grid item xs={3}></Grid>
            <Grid item xs={6}>
                <Suspense fallback={<Spinner/>}>
                    <Pokemon/>
                </Suspense>
            </Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={6}>
                <Item>
                    <Result/>
                </Item>
            </Grid>
            <Grid item xs={3}></Grid>
        </Grid>
    </Box>);
}