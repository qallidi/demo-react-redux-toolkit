import React from "react";
import PropTypes from "prop-types";
import {Chip} from "@mui/material";

export default function BasicChip({label}) {
    return (<Chip label={label} variant="outlined" />);
}

BasicChip.propTypes = {
    label: PropTypes.string
};

BasicChip.defaultProps = {
    label: "initial"
};