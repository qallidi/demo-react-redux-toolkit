import * as React from "react";
import CircularProgress from "@mui/material/CircularProgress";
import Backdrop from "@mui/material/Backdrop";
import {themeColors} from "../assets/theme";

export default function Spinner() {
    return (<Backdrop
        sx={{color: themeColors.white, zIndex: (theme) => theme.zIndex.drawer + 1}}
        open={true}
    >
        <CircularProgress sx={{color: themeColors.cihBlue}}/>
    </Backdrop>);
}