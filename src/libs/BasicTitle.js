import React from "react";
import {Typography} from "@mui/material";
import PropTypes from "prop-types";

export default function BasicTitle({title, variant, component}) {
    return (
        <Typography variant={variant} gutterBottom component={component}>
            {title}
        </Typography>
    );
}

BasicTitle.propTypes = {
    title: PropTypes.string,
    variant: PropTypes.string,
    component: PropTypes.string
};

