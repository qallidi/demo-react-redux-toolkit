import React from "react";
import Button from "@mui/material/Button";
import PropTypes from "prop-types";
import InfoIcon from "@mui/icons-material/Info";
import {themeColors} from "../assets/theme";

/**
 * Basic Button made by qallidi mohammed amine
 * @param icon
 * @param position
 * @param label
 * @param handleClick
 * @param theme
 * @returns {JSX.Element}
 * @constructor
 */
export default function BasicButton({icon, position, label, size, handleClick, theme}) {
    return (<Button size={size} startIcon={position === "start" && icon} endIcon={position === "end" && icon} sx={theme}
                    onClick={handleClick}>
        {label}
    </Button>);
}
/**
 * proTypes definition
 * @type {{handleClick: Requireable<(...args: any[]) => any>, icon: Requireable<ReactElementLike>, theme: Requireable<object>, label: Requireable<string>, position: Requireable<string>, height: Requireable<string>}}
 */
BasicButton.propTypes = {
    label: PropTypes.string,
    size: PropTypes.string,
    icon: PropTypes.element,
    position: PropTypes.oneOf(["end", "start"]),
    handleClick: PropTypes.func,
    theme: PropTypes.object,
    height: PropTypes.string
};
/**
 * defaultProps definition
 * @type {{handleClick: null, icon: JSX.Element, theme: {backgroundColor: string, color: string, "&:hover": {backgroundColor: string}, height: string}, label: string, position: string, height: string}}
 */
BasicButton.defaultProps = {
    label: "Button",
    icon: <InfoIcon/>,
    size: "small",
    position: "start",
    handleClick: null,
    height: "10%",
    theme: {
        color: themeColors.white,
        backgroundColor: themeColors.cihBlue,
        height: "10%",
        "&:hover": {backgroundColor: themeColors.cihBlue}
    }
};

