import * as React from "react";
import {useEffect} from "react";
import PropTypes from "prop-types";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import {useDispatch} from "react-redux";
import {updateCurrentValue} from "../features/pokemon/pokemonSlice";
import {isNil} from "lodash";

export default function BasicSelect({defaultValue, name, items, theme}) {
    const dispatch = useDispatch();
    const [value, setValue] = React.useState("");

    useEffect(() => {
        if (!isNil(value) && value !== "") dispatch(updateCurrentValue(value));
    }, [value]);

    const handleChange = (event) => {
        setValue(event.target.value);
    };

    return (<Box sx={theme}>
        <FormControl fullWidth>
            <InputLabel id="select-label">{name}</InputLabel>
            <Select
                labelId="select-label"
                id="select"
                value={value}
                label={name}
                defaultValue={defaultValue}
                onChange={handleChange}
            >
                {items.map((item) => {
                    return <MenuItem key={item.id} value={item.value}>{item.label}</MenuItem>;
                })}
            </Select>
        </FormControl>
    </Box>);
}

BasicSelect.propTypes = {
    defaultValue: PropTypes.string,
    name: PropTypes.string.isRequired,
    items: PropTypes.array.isRequired,
    theme: PropTypes.object,
};

BasicSelect.defaultProps = {
    defaultValue: "", name: "select", items: [], theme: {minWidth: 120}
};
