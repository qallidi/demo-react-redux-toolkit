import {createAsyncThunk} from "@reduxjs/toolkit";
import {get} from "./Axios";

export const getPokemonByName = createAsyncThunk("pokemon/getPokemonByName", async ({name, rejectWithValue}) => {
    const res = await get(`pokemon/${name}`)
        .then(response => response.data)
        .catch(error => rejectWithValue(error));
    return res;
});