import axios from "axios";

export const GET = "GET";
const URL_BACKEND = "https://pokeapi.co/api/v2/";
// Create Function to handle requests from the backend
export const get = async (ENDPOINT) => {
        const options = {
            url: `${URL_BACKEND}${ENDPOINT}`,
            method: GET,
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json;charset=UTF-8",
            },
          };
        const response = await axios(options);
        return response;
    };
