import  {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {currentValue, pokemonData, statusValue} from "../features/pokemon/pokemonSlice";
import {getPokemonByName} from "../services/pokemonAPI";
import {isNil} from "lodash/lang";

// costum hook to handle fetch data from store and call api
export default function useGetPokemonByName() {
    const dispatch = useDispatch();
    const name = useSelector((state) => currentValue(state));
    const result = useSelector((state) => pokemonData(state));
    const status = useSelector((state) => statusValue(state));

    useEffect(() => {
        if (!isNil(name) && name !== "") dispatch(getPokemonByName({name}));
    }, [name, dispatch]);

    const data = result || [];

    return {data, status};
}