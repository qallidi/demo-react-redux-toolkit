import {createSlice} from "@reduxjs/toolkit";
import {getPokemonByName} from "../../services/pokemonAPI";

const FULFILLED = "fulfilled";
const REJECTED = "rejected";
const PENDING = "pending";

const initialState = {
    currentValue: null, results: [], status: null, errors: []
};

const pokemonSlice = createSlice({
    name: "pokemon", initialState: initialState, reducers: {
        updateCurrentValue: (state, action) => {
            state.currentValue = action.payload;
        }
    }, extraReducers: (builder) => {
        builder
            .addCase(getPokemonByName.pending, (state) => {
                state.status = PENDING;
            })
            .addCase(getPokemonByName.rejected, (state, action) => {
                state.status = REJECTED;
                state.errors = action.error.message;
            })
            .addCase(getPokemonByName.fulfilled, (state, action) => {
                state.status = FULFILLED;
                state.results = action.payload;
            });
    }
});
// export pokemon reducer's actions
export const {updateCurrentValue} = pokemonSlice.actions;
// selectors
export const currentValue = (state) => state.pokemon.currentValue;
export const pokemonData = (state) => state.pokemon.results;
export const statusValue = (state) => state.pokemon.status;

export default pokemonSlice.reducer;