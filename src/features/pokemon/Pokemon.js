import React from "react";
import BasicSelect from "../../libs/BasicSelect";
import BasicTitle from "../../libs/BasicTitle";

const pokemonList = [
    {id: 0, label: "squirtle", value: "squirtle"},
    {id: 1, label: "charmander", value: "charmander"},
    {id: 2, label: "charizard", value: "charizard"},
    {id: 3, label: "raticate", value: "raticate"},
    {id: 4, label: "pidgeot", value: "pidgeot"},
    {id: 5, label: "pidgey", value: "pidgey"},
    {id: 6, label: "blastoise", value: "blastoise"},
    {id: 7, label: "pidgey", value: "pidgey"}];

export default function Pokemon() {
    return (
        <>
            <BasicTitle variant="h4"  component="div" title={"Pokemons"}/>
            <BasicSelect defaultValue={"squirtle"} name={"Pokemon"} items={pokemonList}/>
        </>
    );
}