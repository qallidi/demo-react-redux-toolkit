import React from "react";
import useGetPokemonByName from "../../hooks/useGetPokemonByName";
import BasicTitle from "../../libs/BasicTitle";
import BasicChip from "../../libs/BasicChip";

export default function Result() {
    const {status, data} = useGetPokemonByName();

    return (<>
        <BasicTitle variant="h4" component="div" title={"Result"}/>
        {data.length !== 0 ? (<>
            <BasicChip label={status}/>
            <BasicTitle variant="h5" component="div" title={data.species.name}/>
            <img src={data.sprites.front_shiny} alt={data.species.name}/>
            <img src={data.sprites.back_default} alt={data.species.name}/>
        </>) : null}
    </>);
}